# Importar metodos de un archivo
# from "nombre_archivo" import "nombreMetodo", para importar varios metodos debemos
# separarlos por coma.
from app.menu_agenda import MenuAgenda

# Instanciamos a la clases MenuAgenda
menuAgenda = MenuAgenda()
menuAgenda.mostrar()

"""

from clases.bd_archivos import ArchivoCSV, ArchivoCSVDict

archivoCSV = ArchivoCSV(nombreFolderBD = 'base_datos', nombreArchivo = 'usuarios', delimitador = ';')
data = ['Daniela', 'Cornejo', '9595959595', 'acornejo@gmail.com']
archivoCSV.escribir(data)
archivoCSV.leer()

archivoCSVDict = ArchivoCSVDict(nombreFolderBD = 'base_datos_local', nombreArchivo = 'ejercicioDict', delimitador = ';')
data = [
    {'nombre': 'Wonderful', 'apellido': 'Spam'  },
    {'nombre': 'Wonderful', 'apellido': 'Spam'  },
    {'nombre': 'Wonderful', 'apellido': 'Spam'  },
    {'nombre': 'Wonderful', 'apellido': 'Spam'  },
    {'nombre': 'Wonderful', 'apellido': 'Spam'  },
    {'nombre': 'Wonderful', 'apellido': 'Spam'  }
]

archivoCSVDict.escribir(data)
archivoCSVDict.leer()

"""
# CRUD
# Create Read Update Ddeelte

from DataAccess.connection import Conexion
import datetime

class Usuario:
    
    def __init__(self):
        # Instanciar la conexion a la BD
        self.conexion = Conexion()
    
#    def crear(self, idContacto, nombreContacto,telefonocellContacto,telefonofijoContacto,emailContacto,fechanacimientoContacto,direcciónContacto,fotoContacto):
#        query = f"INSERT INTO `dbagendatelefonica`.`contacto`(`idContacto`,`nombreContacto`,`telefonocellContacto`,`telefonofijoContacto`,`emailContacto`,`fechanacimientoContacto`,`direcciónContacto`,`fotoContacto`)VALUES(<{idContacto: }>,<{nombreContacto: }>,<{telefonocellContacto: }>,<{telefonofijoContacto: }>,<{emailContacto: }>,<{fechanacimientoContacto: }>,<{direcciónContacto: }>,<{fotoContacto: }>);"
#        resultado = self.conexion.ejecutar_query(query)
#        print(resultado)
    
    def listar(self):
        query = 'SELECT * FROM dbagendatelefonica.contacto'
        resultado = self.conexion.ejecutar_query(query)
        filas = resultado.fetchall()
        print(filas)
    



usu = Usuario()

usu.listar()
