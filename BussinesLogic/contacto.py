class AgendaTelefonica:

    contactos = []

    def __init__(self):
        self.contactos.append({"nombre": "Alan 01", "apellido": "cornejo", "telefono": "90000001", "email": "-", "documento": "43487101"})
        self.contactos.append({"nombre": "Alan 02", "apellido": "cornejo", "telefono": "90000002", "email": "-", "documento": "43487102"})
        self.contactos.append({"nombre": "Alan 03", "apellido": "cornejo", "telefono": "90000003", "email": "-", "documento": "43487103"})
        self.contactos.append({"nombre": "Alan 04", "apellido": "cornejo", "telefono": "90000004", "email": "-", "documento": "43487104"})
        self.contactos.append({"nombre": "Alan 05", "apellido": "cornejo", "telefono": "90000005", "email": "-", "documento": "43487105"})
        print('Metodo INIT de la clase AgendaTelefonica')

    def añadirContacto(self, **newContacto):
        self.contactos.append(newContacto)
        return True

    def listarContacto(self):
        return self.contactos

    def buscarContacto(self, documento = '99999999'):
        for contacto in self.contactos:
            if contacto['documento'] == documento:
                return contacto

    def editarContacto(self):
        pass

    def eliminarContacto(self, documento = '99999999'):
        buscar = self.buscarContacto(documento)
        if buscar is not None:
            self.contactos.remove(buscar)
            return True

